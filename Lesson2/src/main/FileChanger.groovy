package main
/**
 * Created with IntelliJ IDEA.
 * User: Vladimir Bezpalchuk
 * Date: 21.10.13
 * Time: 20:25
 * Geekhub Groovy & Grails
 * Lesson 2
 */
class FileChanger {

    public static void main(String[] args) {

        def prefix1 = "a_" // if file starts with 'a_', count all 'a' letters, and rewrite in file
        def prefix2 = "1_" // if file starts with '1_', replace file content by 4x4 random matrix, and sum if diagonal & anti-diagonal
        def prefix3 = "d_" // if file starts with 'd_', replace file content by datetime

        /*Path to the folder, get from command line */
        String path = args[0]

        aMethod(prefix1, path)

        matrixMethod(prefix2, path)

        dateMethod(prefix3, path)

    }

    /**
     * Method count all 'a' character in a file, and write result in a new line
     * Rename file to 'done_' + fileName
     * @param filePath - full path to file
     * @param prefix - prefix of file, will use in regexp
     */
    static void aMethod(def prefix, String filePath) {

        def prefixPattern = "${prefix}.*"

        new File(filePath).eachFileMatch(~/${prefixPattern}/) {
            it.append('\n' + letterCounter(it.absolutePath, 'a'))
            it.renameTo(new File(filePath + "/done_" + it.name))
        }

    }

    /**
     * Method create random 4x4 matrix, compute diagonal and antidiagonal,
     * then write all results to file with deleting old content
     * Rename file to 'done_' + fileName
     * @param filePath - full path to file
     * @param prefix - prefix of file, will use in regexp
     */
    static void matrixMethod(def prefix, String filePath) {

        final int N = 4 //matrix size

        def prefixPattern = "${prefix}.*"

        new File(filePath).eachFileMatch(~/${prefixPattern}/) {
            matrix(it.absolutePath, N)
            it.renameTo(new File(filePath + "/done_" + it.name))
        }

    }

    /**
     * Method write date in a file using format (YYYY-MM-DD HH-MM-SS)
     * Rename file to 'done_' + fileName
     * @param filePath - full path to file
     * @param prefix - prefix of file, will use in regexp
     */
    static void dateMethod(def prefix, String filePath) {

        def prefixPattern = "${prefix}.*"

        def newdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())

        new File(filePath).eachFileMatch(~/${prefixPattern}/) {
            it.write('\n' + newdate)
            it.renameTo(new File(filePath + "/done_" + it.name))
        }

    }

    /**
     * Count number of 'a' character in a target file
     * @param fileName - full path to file
     * @param a - symbol that must be count
     */
    static int letterCounter(def fileName, String a) {

        return new File(fileName).text.findAll { it == a }.size()
    }

    /**
     * Create random NxN integer matrix
     * Compute diagonal and antidiagonal of matrix
     * Write result to target file
     * @param fileName - full path to file
     * @param N - size of matrix
     */
    static void matrix(def fileName, final int N) {

        def A = new int[N][N]

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                A[i][j] = (int) (100 * Math.random())
            }
        }

        def d = diag(A, N)  // diagonals

        File file = new File(fileName)
        file.write('')
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                file.append(A[i][j] + ' ')
            }
            file.append('\n')
        }

        file.append('\n' + d[0])
        file.append('\n' + d[1])

    }

    /**
     * Compute diagonal and antidiagonal of matrix
     * Write result to target file
     * @param A - square matrix NxN
     * @param N - size of matrix
     */
    static int[] diag(def A, int N) {

        int d1 = 0 // diagonal
        int d2 = 0 // antidiagonal

        [0..N - 1, N - 1..0].transpose().each { i, j ->
            d1 = d1 + A[i][i]
            d2 = d2 + A[i][j]
        }

        return [d1, d2]
    }
}
