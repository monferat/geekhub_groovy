package src
/**
 * Created with IntelliJ IDEA.
 * User: Bezpalchuk Vladimir
 * Date: 15.10.13
 * Time: 0:38
 * Geekhub Lesson1
 * Testing in Groovy
 */
class MyTest extends GroovyTestCase {

    void test1() {
        assert ['cat', 'elephant']*.size().sum() == 11
    }

    void test2() {
        assert (1..10).collect{it * 2}[3] == 8 : "We're in trouble, arithmetic is broken"
    }

    void test3() {
        def name = 'World'
        def nameNew = "Hello $name!"
        String nameOrigin = "Hello World!"
        assert nameNew == nameOrigin : "Errore, no World here"
    }
}
