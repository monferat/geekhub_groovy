package xml

/**
 * Created with IntelliJ IDEA.
 * User: Bezpalchuk Vladimir
 * Geekhub Lesson4
 */
import org.codehaus.groovy.transform.GroovyASTTransformationClass
import java.lang.annotation.*

@Retention (RetentionPolicy.SOURCE)
@Target ([ElementType.TYPE])
@GroovyASTTransformationClass (["xml.ToXmlTransformation"])
public @interface ToXml { }
