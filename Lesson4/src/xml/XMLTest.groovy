package xml

/**
 * Created with IntelliJ IDEA.
 * User: Bezpalchuk Vladimir
 * Geekhub Lesson4
 */

@ToXml
class Person {
    String name, phone
}

def person1 = new Person(name:"Chloe",phone:"299123451")
println person1

String xml = person1.toString()
def person2 = Person.createInstanceFrom(xml)
println person2

assert person1.toString() == person2.toString()
