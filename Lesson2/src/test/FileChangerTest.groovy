package test

import static main.FileChanger.*

/**
 * Created with IntelliJ IDEA.
 * User: Vladimir Bezpalchuk
 * Date: 21.10.13
 * Time: 23:45
 * Geekhub Groovy & Grails
 * Lesson 2
 * Tests
 */

class FileChangerTest extends GroovyTestCase {

    /**
     * Testing dateMethod
     */
    void testDateMethod() {

        String path = "/home/vladimir/vladimir/goovy"
        String fname = 'd_file.txt' // test file

        def file = new File(fname)
        file.write(" ")  // write something for creating

        def newdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) // test date

        dateMethod("d_", path)

        def file2 = new File('done_' + fname) // open changed file

        assert file2.text == ('\n' + newdate) // expected low time difference

    }

    /**
     * Testing letterCounter method
     */
    void testLetterCounter() {

        String fname = 'File.txt'
        def f = new File(fname)

        String s = "ssasslpp o ooaafaa" //test string

        f.write(s)

        int count = letterCounter(fname, 'a')

        assert count == 5

    }

    /**
     * Testing diag method
     */
    void testDiag() {

        int N = 4;
        def A = [[1, 0, 0, 2], [0, 1, 2, 0], [0, 2, 1, 0], [2, 0, 0, 1]] // test matrix

        int d1 = 4; // diagonal
        int d2 = 8; // antidiagonal

        def d = [0, 0]
        d = diag(A, N)

        assert d1 == d[0]: 'diagonal not correct'
        assert d2 == d[1]: 'antidiagonal not correct'

    }
}
